# Metadev

### Logins phpldapadmin

- cn=admin,dc=example,dc=org
- admin

### Pour utiliser Docker sur Windows

- PowerShell en mode administrateur : `dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all`
    - Ne pas redémarrer windows tout de suite
- Puis rentrer la commande : `dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all`
    - Redémarrer Windows maintenant
- Télécharger Kali Linux sur le Windows Store pour faire touner WSL sur Windows.
- Dans Windows, aller sur son utilisateur et créer un fichier nommé `.wslconfig` et copier/coller ceci : `[wsl2] memory=4GB`.
- Sur PowerShell en admin : `Restart-Service LxssManager`.
- Télécharger Docker Desktop pour Windows.
- Lancer la commande : `docker-compose up -d` puis visualiser les images directement sur Docker Desktop.

### Pour Owncloud

- Créer un compte admin après avoir "pull" l'image de owncloud.
- Aller dans le market (en haut à gauche), télécharger "LDAP Integration"
- Aller dans paramètres (sous le compte en haut à droite)
- Onglet `Administration` puis `Authentification de l'utilisateur`.
- Rentrer les infos dans "Serveurs, "utilisateurs" ... (+ dans Avancé --> Paramètres du répertoire --> 1er champ : "uid")
- Installer le plug-in PDF viewer
- Et c'est tout bon.

### Pour Zammad

- Créer un compte admin après avoir "pull" l'image de Zammad
- Cliquer sur l'icône des paramètres en bas à gauche, puis aller dans Système -> Intégrations -> LDAP
- Activer LDAP
- Dans Configurer : 
    - entrer comme hôte : ldap://openldap
        - Base DN 	            : dc=example,dc=org
        - Lier l'utilisateur 	: cn=admin,dc=example,dc=org
        - Mot de passe masqué 	: admin
- Associer normalement les attributs LDAP et Zammad
    - sauf Zammad login --> LDAP uid
- Valider la configuration
- Retrouver les users LDAP dans Paramètres : Gérer -> Usagers
- Il est alors possible de gérer les tickets, et de se connecter avec les login des users.

### Pour Moodle

- Suivre ces étapes :
    - Se connecter en admin :
        - login : `user`
        - password : `bitnami`
    - Site Administration --> Plugins --> Authentification --> Manage Authentification --> LDAP Server --> Settings
    - Les champs à modifier dans l'ordre (laisser les autres par défaut) :
        - Host URL : `openldap`
        - Prevent password caching : `YES`
        - Distinguished name : `cn=admin,dc=example,dc=org`
        - Password : `admin`
        - User Type : `posixAccount (rfc2307)`
        - Contexts : `dc=example,dc=org`
        - Search subcontexts : `YES`
        - User attribute : `uid`
        - Member attribute : `memberuid`
        - Password format : `MD5 hash`
        - Partie Data mapping : 
            - Data mapping (Firstname) : `givenName`
            - Update local (Firstname) : `YES`
            - Data mapping (Surname) : `sn`
            - Data mapping (Email address) : `mail`
            - Data mapping (Phone) : `telephoneNumber`
            - Data mapping (Address) : `postalAddress`
        - Cliquer sur "Save changes"
    - Retourner sur Administration --> Plugins --> Authentification
    - Sur la ligne LDAP Server --> "Enable" **ET** "Move up" (petite flèche)
    - Log out et le tour est joué

